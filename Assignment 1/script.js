function loadProvinces(){

    var provArray = ["British Columbia", "Alberta", "Saskatchewan", "Mantioba", "Ontario", "Quebec", "Newfoundland and Labrador", "New Brunswick", "Nova Scotia", "Prince Edward Island", "Yukon", "Northwest Territories", "Nunavut"];
    			
    var optionOutput="";
    optionOutput+="<option name='select' value=''>SELECT</option>";

    for(provIndex=0;provIndex<provArray.length; provIndex++){
    	optionOutput+="<option name='cboProv" + provIndex + "' value='" + provArray[provIndex] + "'>" + provArray[provIndex] + "</option>";
    }
    document.getElementById("provBox").innerHTML=optionOutput;
    alert("Submitted");
}

function validateForm(){
    if(document.forms[0].elements[1].value==""){
    	alert("Please fill in a name");
    	document.forms[0].elements[1].focus();
    	return false;
    }else if(document.getElementById("txtEmail").value==""){
        alert("Please fill in an email");
        document.getElementById("txtEmail").focus();
        return false;
    }else if(document.forms[0].elements[0].selectedIndex==0){
    	alert("Please choose a province");
    	document.forms[0].elements[0].focus();
    	return false;
    }
    document.forms[0].submit(); //validate passed, sumbit the form
}
        