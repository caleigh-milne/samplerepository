Journal 2
October 25


So last week I was having some difficulties around naming functions and parameters. This week I was finally starting to understand JavaScript and jQuery
until we got the Rock Paper Scissors Dynamite assignment. While I was able to get the game to work while everything was in one function I struggled 
with dividing everything into multiple functions. I was able to understand what local and global variables were and where they operated but I really struggled
to understand how to call on local variables from a different function. From what I was able to understand if I want to call variables in different funcitons
I have to place the variable name as a parameter in the function I was to call it. But when I did this I began to get confused with naming again because I was unsure
if the parameter and the variable could have the same name or different names. y thought process was that if I wanted to call the variable TEXT in one function
had to name the parameter TEXT in the other function. I however have no clue if this is actually correct.

Caleigh