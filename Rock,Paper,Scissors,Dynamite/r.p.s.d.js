 //variable to hold game pieces
        	var gamePieces = ["rock", "paper", "scissors", "dynamite"];
//variable to hold the computer's randomly generated choice
            var text;

	
//function to start game at click of button
    		function playGame(){
                //user's prompt to select game piece
    			var chooseGamePieces = prompt("Choose one: rock, paper, scissors, dynamite");

    			text = getRandomGamePiece();
    			//alert(text);
    			var compare = whoWins(chooseGamePieces);

                //pushes who wins into the div "results"
	    		$(document).ready(function(){
	    				$("#results").text(compare);
	    		});	
    		}

        	//computer generates random number associated with array
        	//switch changes the number in array to text and displays
        	//in the paragraph
    		function getRandomGamePiece(text){		
    			var getRandomGamePiece = Math.ceil(Math.random()*gamePieces.length - 1);
    			
    			switch(getRandomGamePiece){
    				case 0:
    				text = "rock";
    				break;
    				case 1:
    				text = "paper";
    				break;
    				case 2:
    				text = "scissors";
    				break;
    				case 3:
    				text = "dynamite";
    				break;
    			}
    			//pushes the computer's choice in the paragraph in the body
    			document.getElementById("compChoice").innerHTML = "Computer chose " + text;
    			//alert(text);
    			return text;
    		}

            //compares the users choice in the prompt with the randomly generated computer's choice
    		function whoWins(chooseGamePieces){
    			//alert(chooseGamePieces + " " + text);
    			var winner;
    			if(chooseGamePieces==text){
    				winner = "You Tie!";
    			}else if(chooseGamePieces=="paper" && text=="rock"){
	    				//document.getElementById("results").innerHTML = "You Won!";
	    				winner = "You Won!";
	    			}else if(chooseGamePieces=="dynamite" && text=="rock"){
	    				winner = "You Won!";
	    			}else if(chooseGamePieces=="rock" && text=="scissors"){
	    				winner = "You Won!";
	    			}else if(chooseGamePieces=="dynamite" && text=="paper"){
	    				winner = "You Won!";
	    			}else if(chooseGamePieces=="scissors" && text=="paper"){
	    				winner = "You Won!";
	    			}else if(chooseGamePieces=="scissors" && text=="dynamite"){
	    				winner = "You Won!";
	    			}else{
	    				winner = "You Lose!"; //if none of the above are met the player loses
	    			}
    			
    			return winner;
    		} 


